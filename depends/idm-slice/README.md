This package provides the translated units of the Slice interfaces
that uses IDM and DUO.  See package 'node-red-contrib-idm' for more
info.
