// -*- mode: c++; coding: utf-8 -*-

module Utils {

    interface RemoteAdapter {
	Object* add(Object* peer);
    };

};
